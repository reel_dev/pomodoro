# Pomodoro Project

## Description

Le projet Pomodoro est une application web qui aide les utilisateurs à gérer leur temps en utilisant la technique Pomodoro. L'application comprend un frontend développé avec Vue.js et une API backend développée avec Spring Boot.

## Structure du projet

Le projet est divisé en deux parties principales :
- **Frontend** : Développé avec Vue.js, il fournit l'interface utilisateur.
- **Backend** : Développé avec Spring Boot, il fournit l'API REST pour gérer les pomodoros.

## Prérequis

Avant de commencer, assurez-vous d'avoir installé les éléments suivants sur votre machine :
- [Node.js 20+](https://nodejs.org/) et npm
- [Java JDK 21+](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Maven](https://maven.apache.org/)
- [Docker](https://docs.docker.com/)

## Installation

### Frontend

1. Accédez au répertoire du frontend :
    ```bash
    cd pomodoro
    ```
2. Installez les dépendances :
    ```bash
    npm install
    ```
3. Lancez le serveur de développement :
    ```bash
    npm run serve
    ```

### Backend

1. Accédez au répertoire du backend :
    ```bash
    cd pomodoroapi
    ```
2. Configurez la base de données dans le fichier `application.properties` ou `application.yml` :
    ```properties
    spring.datasource.url=jdbc:mariadb://localhost:3306/pomodoro_db
    spring.datasource.username=your_username
    spring.datasource.password=your_password
    spring.jpa.hibernate.ddl-auto=update
    ```
3. Lancez un conteneur mariadb. Pensez à adapter l'adresse spring.datasource.url si vous souhaitez le faire entre conteneur !

4. Compilez et démarrez l'application Spring Boot :
    ```bash
    mvn clean install
    mvn spring-boot:run
    ```
L'api sera disponible à /api/v1

## Schémas

![img.png](img.png)

![img_1.png](img_1.png)

## Outils

- Gitlab CI
- vue linter
- npm
- maven
- Docker
- Junit
- Swagger
- Sonarqube
- Jacoco

![img_2.png](img_2.png)