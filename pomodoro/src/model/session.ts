export interface Session {
    id: string,
    sessionType: string
    sessionDate: string
    sessionDuration: number
}