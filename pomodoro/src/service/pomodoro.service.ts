const BASE_URL = "http://localhost:8081/api/v1"

export const getLastPomodoros = async () => {
    try {
        const response = await fetch(BASE_URL + "/pomodoro", {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
            },
        });
        return response.json()
    } catch (error) {
        console.log('Erreur lors de la récupération des derniers pomodoros:', error);
        throw error;
    }
};

export const insertPomodoro = async (pomodoro: any) => {
    // Les options par défaut sont indiquées par *
    const response = await fetch(BASE_URL + "/pomodoro", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(pomodoro),
    });
    return await response.json();
}