import axios from 'axios';

const BASE_URL = "http://localhost:8080";

export function getAll() {
    return axios.get(BASE_URL + "/api/v1/pomodoro")
}

export async function addSession(data: object) : Promise<void>{
    await axios.post(BASE_URL + "/api/v1/pomodoro",data)
        .then(() => {
            return Promise.resolve("coucou")
        })
        .catch(err => console.log(err));
}
