package com.epsi.pomodoroapi.controller;

import com.epsi.pomodoroapi.model.PomodoroModel;
import com.epsi.pomodoroapi.service.PomodoroService;
import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8081/")
@RestController
@RequestMapping("/pomodoro")
public class PomodoroController {

    private final PomodoroService pomodoroService;
    public PomodoroController(PomodoroService pomodoroService){
        this.pomodoroService = pomodoroService;
    }

    @ExceptionHandler(PropertyValueException.class)
    @PostMapping
    public ResponseEntity<Object> insertPomodoro(@RequestBody PomodoroModel pomodoroModel){
        this.pomodoroService.insertPomodoro(pomodoroModel);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public ResponseEntity<Object> getLastPomodoros(){
        return ResponseEntity.of(Optional.ofNullable(pomodoroService.findLastPomodoros()));
    }
}
