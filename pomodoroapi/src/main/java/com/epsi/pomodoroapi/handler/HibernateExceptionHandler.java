package com.epsi.pomodoroapi.handler;

import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class HibernateExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(PropertyValueException.class)
    protected ResponseEntity<Object> handlePropertyValueException(PropertyValueException ex) {
        return new ResponseEntity<>(ex.getPropertyName() + " cannot be null.", HttpStatus.BAD_REQUEST);
    }
}
