package com.epsi.pomodoroapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

@Data
@Entity
public class PomodoroModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private SessionType sessionType;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(nullable = false)
    private LocalDate sessionDate;

    @Column(nullable = false)
    private int sessionDuration;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o instanceof PomodoroModel pomodoroModel) {
            if(this.id == null || pomodoroModel.id == null)
                return false;
            return Objects.equals(this.id.toString(), pomodoroModel.id.toString()) &&
                    Objects.equals(sessionType.name(), pomodoroModel.sessionType.name()) &&
                    Objects.equals(sessionDate.toString(), pomodoroModel.sessionDate.toString()) &&
                    Objects.equals(sessionDuration, pomodoroModel.sessionDuration);
        }
        return false;
    }
    @Override
    public int hashCode() {
        return Objects.hash(id, sessionType, sessionDate, sessionDuration);
    }
}

