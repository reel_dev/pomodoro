package com.epsi.pomodoroapi.model;

public enum SessionType {
    COURTE,
    LONGUE
}
