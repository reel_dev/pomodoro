package com.epsi.pomodoroapi.repository;

import com.epsi.pomodoroapi.model.PomodoroModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PomodoroRepository extends JpaRepository<PomodoroModel, UUID> {
    List<PomodoroModel> findTop10ByOrderBySessionDateDesc();

}
