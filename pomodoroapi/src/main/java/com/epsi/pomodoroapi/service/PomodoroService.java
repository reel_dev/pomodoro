package com.epsi.pomodoroapi.service;

import com.epsi.pomodoroapi.model.PomodoroModel;
import com.epsi.pomodoroapi.model.SessionType;
import com.epsi.pomodoroapi.repository.PomodoroRepository;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PomodoroService {

    private final PomodoroRepository pomodoroRepository;

    public PomodoroService(PomodoroRepository pomodoroRepository){
        this.pomodoroRepository = pomodoroRepository;
    }
    public void insertPomodoro(PomodoroModel pomodoroModel) {
        pomodoroRepository.save(pomodoroModel);
    }

    public List<PomodoroModel> findLastPomodoros() {
        return pomodoroRepository.findTop10ByOrderBySessionDateDesc();
    }
}
