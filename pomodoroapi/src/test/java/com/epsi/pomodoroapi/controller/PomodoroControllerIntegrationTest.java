package com.epsi.pomodoroapi.controller;

import com.epsi.pomodoroapi.model.PomodoroModel;
import com.epsi.pomodoroapi.model.SessionType;
import com.epsi.pomodoroapi.service.PomodoroService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PomodoroControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PomodoroService pomodoroService;

    @InjectMocks
    private PomodoroController pomodoroController;

    private final String api = "/pomodoro";

    @DisplayName("(Integration) Insert pomodoro ok")
    @Test
    void insertPomodoro_ok() throws Exception {
        PomodoroModel pomodoroModel = new PomodoroModel();
        pomodoroModel.setId(UUID.randomUUID());
        pomodoroModel.setSessionType(SessionType.COURTE);
        pomodoroModel.setSessionDate(LocalDate.of(2020,12,12));

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        String pomodoroJson = objectMapper.writeValueAsString(pomodoroModel);

        doNothing().when(pomodoroService).insertPomodoro(pomodoroModel);

        mockMvc.perform(post(api)
                        .contentType("application/json")
                        .content(pomodoroJson))
                .andExpect(status().isCreated());
    }

    @Test
    void getLastPomodoroOk() throws Exception {
        when(pomodoroService.findLastPomodoros()).thenReturn(new ArrayList<>());
        mockMvc.perform(get(api))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }
}
