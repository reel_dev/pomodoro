package com.epsi.pomodoroapi.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class PomodoroModelTest {

    @Test
    void equals_Ok(){
        PomodoroModel pomodoroModel = new PomodoroModel();
        PomodoroModel pomodoroModel1 = new PomodoroModel();
        PomodoroModel pomodoroModel2 = new PomodoroModel();

        pomodoroModel.setId(UUID.randomUUID());
        pomodoroModel.setSessionType(SessionType.LONGUE);
        pomodoroModel.setSessionDate(LocalDate.of(2010,1,1));
        pomodoroModel.setSessionDuration(1000);

        pomodoroModel1.setId(pomodoroModel.getId());
        pomodoroModel1.setSessionType(SessionType.LONGUE);
        pomodoroModel1.setSessionDate(LocalDate.of(2010,1,1));
        pomodoroModel1.setSessionDuration(1000);

        pomodoroModel2.setId(pomodoroModel.getId());
        pomodoroModel2.setSessionType(SessionType.LONGUE);
        pomodoroModel2.setSessionDate(LocalDate.of(2010,1,1));
        pomodoroModel2.setSessionDuration(1000);

        assertEquals(pomodoroModel, pomodoroModel1);
        assertEquals(pomodoroModel1, pomodoroModel);
        assertEquals(pomodoroModel2, pomodoroModel);
        assertEquals(pomodoroModel1.hashCode(), pomodoroModel.hashCode());

        pomodoroModel.setId(null);
        pomodoroModel1.setId(null);
        assertNotEquals(pomodoroModel1, pomodoroModel);

        pomodoroModel.setId(UUID.randomUUID());
        pomodoroModel1.setId(pomodoroModel.getId());
        pomodoroModel.setSessionType(SessionType.COURTE);
        assertNotEquals(pomodoroModel1, pomodoroModel);
        pomodoroModel.setSessionType(SessionType.LONGUE);
        pomodoroModel.setSessionDuration(50);
        assertNotEquals(pomodoroModel1, pomodoroModel);
        pomodoroModel.setSessionDuration(1000);
        pomodoroModel.setSessionDate(LocalDate.of(2011,1,1));
        assertNotEquals(pomodoroModel1, pomodoroModel);
        assertNotEquals(pomodoroModel, new Object());
        pomodoroModel.setId(null);
        assertNotEquals(pomodoroModel1, pomodoroModel);
    }
}
