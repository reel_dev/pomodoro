package com.epsi.pomodoroapi.service;

import com.epsi.pomodoroapi.model.PomodoroModel;
import com.epsi.pomodoroapi.model.SessionType;
import com.epsi.pomodoroapi.repository.PomodoroRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class PomodoroServiceTest {

    @Mock
    private PomodoroRepository pomodoroRepository;

    private PomodoroService pomodoroService;

    @BeforeEach
    void setup(){
        pomodoroService = new PomodoroService(pomodoroRepository);
    }
    @Test
    void insertion_ok() {
        PomodoroModel pomodoroModel = new PomodoroModel();
        pomodoroModel.setId(UUID.randomUUID());
        pomodoroModel.setSessionType(SessionType.LONGUE);
        pomodoroModel.setSessionDate(LocalDate.of(2020,12,12));
        when(pomodoroRepository.save(pomodoroModel)).thenReturn(pomodoroModel);
        assertDoesNotThrow(() -> pomodoroService.insertPomodoro(pomodoroModel));
    }

    @Test
    void get_ok() {
        PomodoroModel pomodoroModel = new PomodoroModel();
        pomodoroModel.setId(UUID.randomUUID());
        pomodoroModel.setSessionType(SessionType.LONGUE);
        pomodoroModel.setSessionDate(LocalDate.of(2020,12,12));
        when(pomodoroRepository.findTop10ByOrderBySessionDateDesc()).thenReturn(List.of(pomodoroModel));
        List<PomodoroModel> pomodoroModels = pomodoroService.findLastPomodoros();
        assertEquals(pomodoroModels.getFirst(), pomodoroModel);
    }
}